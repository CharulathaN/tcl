#### Makefile for I-Class base-sim ####

ifeq (, $(wildcard ./old_vars))
	old_define_macros = ""
else
	include ./old_vars
	endif

CONFIG?=soc_config.inc

include $(CONFIG)
FPGA:=xcvu095-ffva2104-2-e
JTAG_TYPE:=JTAG_EXTERNAL
#ISA=RV64IMAFDC
SYNTHTOP=mk_core
JOBS=4
#XLEN=64

SHAKTI_HOME=$(PWD)
export SHAKTI_HOME


TOP_MODULE:=mk_core
TOP_FILE:=core.bsv
TOP_DIR:=../src/core
# TOP_MODULE:=mk_core
# TOP_FILE:=core.bsv
# TOP_DIR:=../src/core
WORKING_DIR := $(shell pwd)

# open bsc changes 
BSC_DIR := $(shell which bsc)
BSC_VDIR:=$(subst /bin/bsc,/,${BSC_DIR})bin/../lib/Verilog/
BSC_VIVADODIR:=$(subst /bin/bsc,/,${BSC_DIR})bin/../lib/Verilog.Vivado/


# ------------------ based on the config generate define macros for bsv compilation --------------#
# isa
ifneq (,$(findstring RV64,$(ISA)))
  override define_macros += -D RV64=True
  XLEN=64
endif
ifneq (,$(findstring RV32,$(ISA)))
  override define_macros += -D RV32=True
  XLEN=32
endif
ifneq (,$(findstring A,$(ISA)))
  override define_macros += -D atomic=True
endif
ifneq (,$(findstring F,$(ISA)))
  override define_macros += -D spfpu=True
  FLOAT=--float
endif
ifneq (,$(findstring D,$(ISA)))
  override define_macros += -D dpfpu=True
  FLOAT=--float
endif
ifneq (,$(findstring F,$(ISA)))
  define_macros += -D spfpu=True
endif
ifneq (,$(findstring D,$(ISA)))
  define_macros += -D dpfpu=True
endif
ifneq (,$(findstring C,$(ISA)))
  override define_macros += -D compressed=True
endif
ifneq (,$(findstring M,$(ISA)))
  override define_macros += -D muldiv=True
endif

# simulation
ifeq ($(SYNTH),SIM)
  override define_macros += -D simulate=True
  override define_macros += -D rtldump=True
  # include changes for new spike in rtl.dump
  ifeq ($(NEW_SPIKE), enable)
    override define_macros += -D new_spike=True
  endif

  # include exceptions in rtl.dump
  ifeq ($(COCOTB_RTLDUMP), enable)
    override define_macros += -D cocotb_rtldump=True
  endif

  # enable rtl.stats, rtl.time
  ifeq ($(ENABLE_STATS), enable)
    override define_macros += -D enable_stats=True
  endif
endif

ifeq ($(STATIC_CHECK), enable)
  override define_macros += -D static_check=True
endif

# enable perfmonitors
ifeq ($(PERFMONITORS), enable)
  override define_macros += -D perfmonitors=True
endif

# enable csr counters
ifeq ($(CSRCOUNTERS), enable)
  override define_macros += -D csr_grp4=True
endif

# enable csr event registers
ifeq ($(CSREVENTS), enable)
  override define_macros += -D csr_events_grp4=True
endif

# simple bpred
ifeq ($(BPRED_SIMPLE), enable)
  override define_macros+= -D bpred_simple=True

  # backing pred
  ifeq ($(BPRED_BACKING), enable)
    override define_macros+= -D bpred_backing=True

    ifeq ($(BPRED_BACKING_TAGE), enable)
      override define_macros+= -D bpred_backing_tage=True

      ifeq ($(BPRED_TAGE_HASH_COMPLEX), enable)
        override define_macros+= -D bpred_tage_hash_complex=True
      endif

      ifeq ($(BPRED_TAGE_HIST_COMPLEX), enable)
        override define_macros+= -D bpred_tage_hist_complex=True
      endif

      ifeq ($(HASH_TAGE_SIMPLE_FOLD), enable)
        override define_macros+= -D hash_tage_simple_fold=True
      endif

      ifeq ($(HASH_TAGE_SIMPLE), enable)
        override define_macros+= -D hash_tage_simple=True
      endif

      ifeq ($(HASH_TAGE_REVERSE_FOLD), enable)
        override define_macros+= -D hash_tage_reverse_fold=True
      endif

      ifeq ($(HASH_TAGE_SHIFT_FOLD), enable)
        override define_macros+= -D hash_tage_shift_fold=True
      endif

      ifeq ($(HASH_TAGE_PATH_FOLD), enable)
        override define_macros+= -D hash_tage_path_fold=True
      endif

      ifeq ($(HASH_TAGE_TAG_SIMPLE), enable)
        override define_macros+= -D hash_tage_tag_simple=True
      endif

      ifeq ($(HASH_TAGE_TAG_UL), enable)
        override define_macros+= -D hash_tage_tag_ul=True
      endif
    endif # tage

    # multi target
    ifeq ($(BPRED_BACKING_MULTITGT), enable)
      override define_macros+= -D bpred_backing_multitgt=True

      ifeq ($(BPRED_BACKING_BTP_PARTIAL), enable)
        override define_macros+= -D bpred_backing_btp_partial=True
      endif

      ifeq ($(BPRED_IBTB_ALL_PATH_HISTORY), enable)
        override define_macros+= -D bpred_ibtb_all_path_history=True
      endif

    endif # multi target

  endif # backing pred

  ifeq ($(BPRED_SPLIT_OPT), enable)
    override define_macros+= -D bpred_split_opt=True
  endif
endif # simple bpred

# fetch
ifeq ($(FETCH_SPLIT), enable)
    override define_macros+= -D fetch_split=True
endif

# decode
ifeq ($(DECODE_REDIR_STRICT), enable)
    override define_macros+= -D decode_redir_strict=True
endif

# dependence predictor
ifeq ($(DEP_SPECULATION), enable)
    override define_macros+= -D misspeculation_enable=True

  ifeq ($(DEP_CONSERVATIVE), enable)
    override define_macros+= -D mdp1_conservative=True
  endif

  ifeq ($(MDP1), enable)
    override define_macros+= -D mdp1=True
  endif

  ifeq ($(MDP1_BRAM), enable)
    override define_macros+= -D mdp1_bram=True
  endif

  ifeq ($(MDP2), enable)
    override define_macros+= -D mdp2=True
  endif
endif

# split prf
ifeq ($(SPLIT_PRF), enable)
    override define_macros+= -D split_prf=True
endif

# enable age priority in IW
ifeq ($(IW_AGE_PRIORITY), enable)
  override define_macros += -D iw_age_priority=True
endif

# check
ifeq ($(CONSERVATIVE), enable)
    override define_macros+= -D segmented_dispatch=True
    override define_macros+= -D one_hot_fu_type=True
else ifeq ($(CONSERVATIVE), disable)
    override define_macros+= -D writeback_flush_enable=True
endif

# enable prefetcher
ifeq ($(PREFETCH), enable)
  override define_macros += -D pref=True

  # select next line prefetcher
  ifeq ($(NEXTLINE), enable)
    override define_macros += -D nextline=True
  endif

  # select reg based stride prefetcher
  ifeq ($(REGSTRIDE), enable)
    override define_macros += -D regstride=True
  endif

  # select reg based line stride prefetcher
  ifeq ($(LINESTRIDE), enable)
    override define_macros += -D linestride=True
  endif

  # select bram2 based stride prefetcher
  ifeq ($(BRAM2STRIDE), enable)
    override define_macros += -D bram2stride=True
  endif
endif

# fpu
ifeq ($(FPU_HIERARCHICAL), enable)
    override define_macros+= -D fpu_hierarchical=True
endif

ifeq ($(FPU_PIPELINED), enable)
    override define_macros+= -D fpu_pipelined=True
endif

ifeq ($(DENORMAL_SUPPORT), enable)
    override define_macros+= -D denormal_support=True
endif

ifeq ($(TB_SUPPORT), enable)
    override define_macros+= -D tb_support=True
endif

# other
ifeq ($(CLINT), enable)
  override define_macros += -D clint=True
endif
ifeq ($(BOOTROM), enable)
  override define_macros += -D BOOTROM=True
endif
ifeq ($(COREFABRIC), AXI4Lite)
  override define_macros += -D CORE_AXI4Lite=True
endif
ifeq ($(USERTRAPS), enable)
  override define_macros += -D usertraps=True
endif
ifeq ($(USER), enable)
  override define_macros += -D user=True
endif
ifeq ($(RTLDUMP), enable)
  override define_macros += -D rtldump=True
endif
ifneq ($(SUPERVISOR),  none)
  override define_macros += -D supervisor=True -D $(SUPERVISOR)
endif
ifeq ($(ASSERTIONS), enable)
  override define_macros += -D ASSERT=True
endif
ifeq ($(ICACHE), enable)
  override define_macros += -D icache=True
endif
ifeq ($(DCACHE), enable)
  override define_macros += -D dcache=True
endif
ifeq ($(ECC), enable)
  override define_macros += -D ECC=True
endif
ifeq ($(PMP), enable)
    override define_macros += -D pmp=True
endif
ifeq ($(ARITH_TRAP), enable)
    override define_macros += -D arith_trap=True
endif
ifeq ($(DEBUG), enable)
    override define_macros += -D debug=True
endif
ifeq ($(OPENOCD), enable)
    override define_macros += -D openocd=True
endif
ifeq ($(COVERAGE), none)
else ifeq ($(COVERAGE),all)
  coverage := --coverage
endif
ifeq ($(TRACE), enable)
  trace := --trace
endif

# check
ifeq ($(ICACHE), enable)
  define_macros += -D icache=True -D cache_control=True
endif
ifeq ($(DCACHE), enable)
    define_macros += -D dcache=True -D cache_control=True
endif

VCS_MACROS =  +define+BSV_RESET_FIFO_HEAD=True +define+BSV_RESET_FIFO_ARRAY=True
VERILATOR_FLAGS += -DVERBOSE
ifneq (0,$(VERBOSITY))
	VCS_MACROS += +define+VERBOSE=True
endif

override define_macros += -D iclass=True -D VERBOSITY=$(VERBOSITY) -D LOG_START=$(LOG_START) -D vaddr=$(VADDR) -D caddr=$(CADDR) -D xlen=$(XLEN) \
									-D resetpc=$(RESETPC) -D paddr=$(PADDR) \
								 	-D iwords=$(IWORDS) -D iblocks=$(IBLOCKS) -D iways=$(IWAYS) \
									-D isets=$(ISETS) -D ifbsize=$(IFBSIZE) -D irepl=$(IREPL) \
									-D icachereset=$(IRESET) -D iesize=$(IESIZE) -D idbanks=$(IDBANKS) \
									-D itbanks=$(ITBANKS) -D causesize=$(CAUSESIZE) -D ibuswidth=$(IBUSWIDTH) \
									-D ifence=True -D icache=True -D bpureset=1 -D icache_onehot=$(ICACHE_ONE_HOT) \
									-D asidwidth=$(ASIDWIDTH) -D itlbsize=$(ITLBSIZE) -D dtlbsize=$(DTLBSIZE) -D  DTVEC_BASE=$(DTVEC) \
									-D dwords=$(DWORDS) -D desize=$(DESIZE) \
									-D mem_add_latency=$(MEM_ADD_LATENCY)

# ------------------ Include directories for bsv compilation ------------------------------------- #
BSVDEFAULT:=.:%/Libraries:
VARIABLE := $(BSVDEFAULT)$(shell sed 's/^/:..\//g' ../bsvpath)
BSVINCDIR := $(shell echo $(VARIABLE) | sed 's/ //g' )
# ------------------------------------------------------------------------------------------------ #

# ----------------- Setting up flags for verilator ----------------------------------------------- #
ifeq ($(VERILATESIM), fast)
	verilate_fast := OPT_SLOW="-O3" OPT_FAST="-O3"
endif
VERILATOR_FLAGS += -O3 -LDFLAGS "-static" --x-assign fast --x-initial fast --trace\
--noassert sim_main.cpp --bbox-sys -Wno-STMTDLY -Wno-UNOPTFLAT -Wno-WIDTH \
-Wno-lint -Wno-COMBDLY -Wno-INITIALDLY --autoflush $(coverage) $(trace) --threads $(THREADS) \
-DBSV_RESET_FIFO_HEAD -DBSV_RESET_FIFO_ARRAY --output-split 20000 --output-split-ctrace 10000
# ------------------------------------------------------------------------------------------------ #

# ---------------- Setting the variables for bluespec compile  --------------------------------- #
BSC_CMD:= bsc -u -verilog -elab
BSVCOMPILEOPTS:= +RTS -K40000M -RTS -check-assert  -keep-fires -opt-undetermined-vals \
									$(suppresswarn) -remove-false-rules -remove-empty-rules -remove-starved-rules \
									-remove-dollar -unspecified-to X -show-schedule -show-module-use
BSVLINKOPTS:=-parallel-sim-link 8 -keep-fires
VERILOGDIR:=./verilog/
BSVBUILDDIR:=./bsv_build/
BSVOUTDIR:=./bin
ifeq (, $(wildcard ${TOOLS_DIR}/shakti-tools/insert_license.sh))
  VERILOG_FILTER:= -verilog-filter ./rename_translate.sh
else
  VERILOG_FILTER:= -verilog-filter ${TOOLS_DIR}/shakti-tools/insert_license.sh \
									 -verilog-filter ./rename_translate.sh
	VERILOGLICENSE:= cp ${TOOLS_DIR}/shakti-tools/IITM_LICENSE.txt ${VERILOGDIR}/LICENSE.txt
endif
# ------------------------------------------------------------------------------------------------ #

# ------------------------------------- Makefile TARGETS ----------------------------------------- #
default: generate_verilog link_verilator simulate
gdb: generate_verilog link_verilator_gdb generate_boot_files

.PHONY: help
help: ## This help dialog.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//' | column	-c2 -t -s :

check-env:
	@if test -z "$(BSC_DIR)"; then echo "BSC_DIR variable not set"; exit 1; else echo "BSC_DIR is set as $(BSC_DIR)"; fi;

check-py:
	@if ! [ -a /usr/bin/python3 ] ; then echo "Python3 is required in /usr/bin to run AAPG" ; exit 1; fi;

.PHONY: check-restore
check-restore:
	@if [ "$(define_macros)" != "$(old_define_macros)" ];	then	make clean ;	fi;

.PHONY: update_xlen
update_xlen:
	@echo "XLEN=128" > verification/dts/Makefile.inc

.PHONY:  compile_bluesim
compile_bluesim: check-restore check-env
	@echo "Compiling $(TOP_MODULE) in Bluesim..."
	@mkdir -p $(BSVBUILDDIR) 
	@echo "old_define_macros = $(define_macros)" > old_vars
	bsc -u -sim -simdir $(BSVBUILDDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR) $(define_macros)\
  $(BSVCOMPILEOPTS) -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE)\
	|| (echo "ERROR: BSC COMPILE ERROR"; exit 1)
	@echo "Compilation finished"

.PHONY: link_bluesim
link_bluesim:check-env
	@echo "Linking $(TOP_MODULE) in Bluesim..."
	@mkdir -p $(BSVOUTDIR)
	bsc -e $(TOP_MODULE) -sim -o $(BSVOUTDIR)/out -simdir $(BSVBUILDDIR) -p $(BSVINCDIR) -bdir\
  $(BSVBUILDDIR) $(BSVLINKOPTS) 2>&1 | tee bsv_link.log || (echo "ERROR: BSC LINK ERROR"; exit 1)
	@echo Linking finished

.PHONY: simulate
simulate: ## Simulate the 'out' executable
	@echo Simulation...
	@exec ./$(BSVOUTDIR)/out +fullverbose
	@echo Simulation finished

.PHONY: generate_verilog
generate_verilog: check-restore check-env
	@echo Compiling $(TOP_MODULE) in verilog ...
	@mkdir -p $(BSVBUILDDIR);
	@mkdir -p $(VERILOGDIR);
	@echo "old_define_macros = $(define_macros)" > old_vars
	$(BSC_CMD) -vdir $(VERILOGDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR)\
  $(define_macros) $(BSVCOMPILEOPTS) $(VERILOG_FILTER) \
  -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE)  || (echo "BSC COMPILE ERROR"; exit 1)
	@cp ${BSC_VIVADODIR}/RegFile.v ${VERILOGDIR}
	@cp ${BSC_VIVADODIR}/BRAM2BELoad.v ${VERILOGDIR}
	@cp ${BSC_VIVADODIR}/BRAM2BE.v ${VERILOGDIR}
	@cp ${BSC_VIVADODIR}/BRAM2.v ${VERILOGDIR}
	@cp ./common_verilog/bram_1r1w.v ${VERILOGDIR}
	@cp ./common_verilog/bram_1rw.v ${VERILOGDIR}
	@cp ./common_verilog/BRAM1Load.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/FIFO2.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/FIFO1.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/FIFO10.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/RevertReg.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/FIFO20.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/FIFOL1.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SyncFIFO.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/Counter.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SizedFIFO.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/ResetEither.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/MakeReset0.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/ClockInverter.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SyncReset0.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SyncRegister.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/MakeClock.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/UngatedClockMux.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/ClockInverter.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/MakeResetA.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/MakeReset0.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SyncResetA.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/ResetEither.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SyncHandshake.v ${VERILOGDIR}
	@cp ${BSC_VDIR}/SyncFIFO1.v ${VERILOGDIR}
	@cp common_verilog/signedmul.v ${VERILOGDIR}
	@$(VERILOGLICENSE)
#ifeq ($(SYNTH), SIM)
#  ifeq ($(MUL), fpga)
#    ifneq (,$(findstring M,$(ISA)))
#		  @cp fpga/manage_ip/manage_ip.srcs/sources_1/ip/multiplier/multiplier_sim_netlist.v\
#    	./verilog/multiplier.v || (echo "ERROR: PLEASE BUILD VIVADO IP FIRST"; exit 1)
#  #		@cp fpga/manage_ip/manage_ip.srcs/sources_1/ip/divider/divider_sim_netlist.v\
#    	./verilog/divider.v || (echo "ERROR: PLEASE BUILD VIVADO IP FIRST"; exit 1)
#    endif
#  endif
#endif
	@echo Compilation finished

.PHONY: link_vcs
link_vcs: 
	@mkdir -p bin
	@rm -rf bin/*
	@vcs -full64 -l vcs_compile.log -sverilog +vpi +v2k -lca +define+TOP=$(TOP_MODULE) \
	+define+BSV_TIMESCALE=1ns/1ps +cli+4 +libext+.v +notimingcheck\
  ${XILINX_VIVADO}/data/verilog/src/glbl.v \
	-y $(VERILOGDIR)/ -y ${BSC_VDIR}/ \
	-y ${XILINX_VIVADO}/data/verilog/src/unisims +libext+.v \
	-y ${XILINX_VIVADO}/data/verilog/src/unimacro +libext+.v \
	-y ${XILINX_VIVADO}/data/verilog/src/retarget +libext+.v \
	${BSC_VDIR}/main.v -o out
	@mv csrc out* bin

.PHONY: link_ncverilog
link_ncverilog: 
	@echo "Linking $(TOP_MODULE) using ncverilog..."
	@rm -rf work include bin/work
	@mkdir -p bin 
	@mkdir work
	@echo "define work ./work" > cds.lib
	@echo "define WORK work" > hdl.var
	@ncvlog -64BIT -sv -cdslib ./cds.lib -hdlvar ./hdl.var +define+TOP=$(TOP_MODULE) \
	${BSC_VDIR}/main.v ${XILINX_VIVADO}/data/verilog/src/glbl.v \
	-y $(VERILOGDIR)/ \
	-y ${BSC_VDIR}/\
	-y ${XILINX_VIVADO}/data/verilog/src/ \
	-y ${XILINX_VIVADO}/data/verilog/src/unisims \
	-y ${XILINX_VIVADO}/data/verilog/src/unimacro \
	-y ${XILINX_VIVADO}/data/verilog/src/retarget 
	@ncelab  -cdslib ./cds.lib -hdlvar ./hdl.var work.main -timescale 1ns/1ps
	@echo 'ncsim -cdslib ./cds.lib -hdlvar ./hdl.var work.main #> /dev/null' > $(BSVOUTDIR)/out
	@mv work cds.lib hdl.var $(BSVOUTDIR)/
	@chmod +x $(BSVOUTDIR)/out
	@echo Linking finished

.PHONY: link_irun
link_irun:
	@irun -define TOP=mkTbSoC -timescale 1ns/1ps $(VERILOGDIR)/main.v \
	${XILINX_VIVADO}/data/verilog/src/glbl.v \
	-y $(VERILOGDIR)/ \
	-y ${BSC_VDIR}/\
	-y ${XILINX_VIVADO}/data/verilog/src/ \
	-y ${XILINX_VIVADO}/data/verilog/src/unisims \
	-y ${XILINX_VIVADO}/data/verilog/src/unimacro \
	-y ${XILINX_VIVADO}/data/verilog/src/retarget 
	
	

.PHONY: link_msim
link_msim: 
	@echo "Linking $(TOP_MODULE) using modelsim..."
	@rm -rf work* bin/*
	@mkdir -p bin 
	vlib work
	vlog -work work +libext+.v+.vqm -y ./src/bfm -y $(VERILOGDIR) -y ${BSC_VDIR} +define+TOP=$(TOP_MODULE) ${BSC_VDIR}/main.v ./$(VERILOGDIR)/$(TOP_MODULE).v  > compile_log
	mv compile_log ./$(BSVOUTDIR)
	mv work ./$(BSVOUTDIR)
	echo 'vsim -quiet -novopt -lib work -do "run -all; quit" -c main' > $(BSVOUTDIR)/out
	@chmod +x $(BSVOUTDIR)/out
	@echo Linking finished


.PHONY: link_verilator
link_verilator: ## Generate simulation executable using Verilator
	@echo "Linking $(TOP_MODULE) using verilator"
	@mkdir -p $(BSVOUTDIR) obj_dir
	@echo "#define TOPMODULE V$(TOP_MODULE)" > sim_main.h
	@echo '#include "V$(TOP_MODULE).h"' >> sim_main.h
	verilator $(VERILATOR_FLAGS) --cc $(TOP_MODULE).v -y $(VERILOGDIR) -y common_verilog --exe
	@ln -f -s ../sim_main.cpp obj_dir/sim_main.cpp
	@ln -f -s ../sim_main.h obj_dir/sim_main.h
	make $(verilate_fast) VM_PARALLEL_BUILDS=1 -j8 -C obj_dir -f V$(TOP_MODULE).mk
	@cp obj_dir/V$(TOP_MODULE) $(BSVOUTDIR)/out

.PHONY: link_verilator_gdb
link_verilator_gdb: ## Generate simulation executable using Verilator and VPI for GDB
	@echo "Linking Verilator With the Shakti RBB Vpi"
	@mkdir -p $(BSVOUTDIR) obj_dir
	@echo "#define TOPMODULE V$(TOP_MODULE)_edited" >sim_main.h
	@echo '#include "V$(TOP_MODULE)_edited.h"' >> sim_main.h
	@sed  -f devices/jtagdtm/sed_script.txt  $(VERILOGDIR)/$(TOP_MODULE).v > tmp1.v
	@cat  devices/jtagdtm/verilator_config.vlt \
	      devices/jtagdtm/vpi_sv.v \
	      tmp1.v                         > $(VERILOGDIR)/$(TOP_MODULE)_edited.v
	@rm   -f  tmp1.v
	verilator $(VERILATOR_FLAGS) --threads-dpi all --cc $(TOP_MODULE)_edited.v --exe sim_main.cpp devices/jtagdtm/RBB_Shakti.c -y $(VERILOGDIR)  
	@ln -f -s ../test_soc/sim_main.cpp obj_dir/sim_main.cpp
	@ln -f -s ../sim_main.h obj_dir/sim_main.h
	@ln -f -s ./devices/jtagdtm/RBB_Shakti.c obj_dir/RBB_Shakti.c
	@echo "INFO: Linking verilated files"
	make $(VERILATOR_SPEED) VM_PARALLEL_BUILDS=1 -j4 -C obj_dir -f V$(TOP_MODULE)_edited.mk
	@cp obj_dir/V$(TOP_MODULE)_edited $(BSVOUTDIR)/out
	@cp gdb_setup/code.mem $(BSVOUTDIR)/code.mem
	@echo Linking finished


.PHONY: link_iverilog
link_iverilog: 
	@echo "Linking $(TOP_MODULE) using iverilog..."
	@mkdir -p bin 
	@iverilog -v -o bin/out -Wall -y ./src/bfm -y $(VERILOGDIR) -y ${BSC_VDIR}/ -DTOP=$(TOP_MODULE) ${BSC_VDIR}/main.v .$(VERILOGDIR)/$(TOP_MODULE).v
	@echo Linking finished

.PHONY: ip_build
ip_build: 
	vivado -mode tcl -notrace -source $(SHAKTI_HOME)/src/tcl/create_ip_project.tcl -tclargs $(FPGA) $(ISA) $(JOBS) \
	       	|| (echo "Could \ not create IP project"; exit 1)
#       @vivado -mode tcl -notrace -source $(SHAKTI_HOME)/src/tcl/create_multiplier.tcl -tclargs $(XLEN) $(MULSTAGES) ||\
(echo "Could not create Multiplier IP"; exit 1)
#	@vivado -mode tcl -notrace -source $(SHAKTI_HOME)/src/tcl/create_nexys4_mig.tcl ||\
(echo "Could not create NEXYS4DDR-MIG  IP"; exit 1)
#	@vivado -mode tcl -notrace -source $(SHAKTI_HOME)/src/tcl/create_divider.tcl -tclargs $(XLEN) $(DIVSTAGES) ||\
(echo "Could not create Divider IP"; exit 1)

.PHONY: vivado_build
vivado_build: 
	@vivado -mode tcl -source $(SHAKTI_HOME)/src/tcl/create_project.tcl -tclargs $(SYNTHTOP) $(FPGA) $(ISA) $(JTAG_TYPE) \
	       	|| (echo "Could not create core project"; exit 1)
	@vivado -mode tcl -source $(SHAKTI_HOME)/src/tcl/run.tcl -tclargs $(JOBS) \
	       	|| (echo "ERROR: While running synthesis")
      

.PHONY: regress
regress: ## To run regressions on the core.
	@SHAKTI_HOME=$$PWD perl -I$(SHAKTI_HOME)/verification/verif-scripts $(SHAKTI_HOME)/verification/verif-scripts/makeRegress.pl $(opts)

.PHONY: test
test: ## To run a single riscv-test on the core.
	@SHAKTI_HOME=$$PWD CONFIG_LOG=0 perl -I$(SHAKTI_HOME)/verification/verif-scripts $(SHAKTI_HOME)/verification/verif-scripts/makeTest.pl $(opts)

.PHONY: torture
torture: ## To run riscv-tortur on the core.
	@SHAKTI_HOME=$$PWD perl -I$(SHAKTI_HOME)/verification/verif-scripts $(SHAKTI_HOME)/verification/verif-scripts/makeTorture.pl $(opts)

.PHONY: aapg
aapg: ## to generate and run aapf tests
	@SHAKTI_HOME=$$PWD perl -I$(SHAKTI_HOME)/verification/verif-scripts $(SHAKTI_HOME)/verification/verif-scripts/makeAapg.pl $(opts)

.PHONY: csmith
csmith: ## to generate and run csmith tests
	@SHAKTI_HOME=$$PWD perl -I$(SHAKTI_HOME)/verification/verif-scripts $(SHAKTI_HOME)/verification/verif-scripts/makeCSmith.pl $(opts)

.PHONY: generate_boot_files
generate_boot_files: update_xlen
	@mkdir -p bin
	@cd verification/dts/; make;
	@cp verification/dts/boot.hex  bin/bootfile


.PHONY: patch
patch:
	@cd $(SHAKTI_HOME)/verification/riscv-tests/env && git apply $(SHAKTI_HOME)/verification/patches/riscv-tests-shakti-iclass.patch

.PHONY: unpatch
unpatch:
	@cd $(SHAKTI_HOME)/verification/riscv-tests/env && git apply -R $(SHAKTI_HOME)/verification/patches/riscv-tests-shakti-iclass.patch

.PHONY: merge_cov
merge_cov:
	cd $(SHAKTI_HOME)/verification/workdir && ln -s $(SHAKTI_HOME)/verilog verilog
	verilator_coverage --write merged.dat $(SHAKTI_HOME)/verification/workdir/*/*/*/coverage.dat	
	verilator_coverage --annotate logs merged.dat
	verilator_coverage --rank $(SHAKTI_HOME)/verification/workdir/*/*/*/coverage.dat	
	rm -rf $(SHAKTI_HOME)/verification/workdir/*/*/*/coverage.dat	

.PHONY: yml
yml:
	@SHAKTI_HOME=$$PWD python3 $(SHAKTI_HOME)/verification/verif-scripts/gen_yml.py $(opts)

.PHONY: clean
clean:
	rm -rf $(BSVBUILDDIR) *.log $(BSVOUTDIR) obj_dir
	rm -f *.jou rm *.log *.mem old_vars log

clean_verilog: clean
	rm -rf verilog/
	rm -rf fpga/
	rm -rf INCA*
	rm -rf work
	rm -f ./ncvlog.*
	rm -f irun.*

clean_verif:
	rm -rf verification/workdir/*
	rm -rf verification/riscv-torture/output/riscv-torture

restore: clean_verilog

.PHONY: release 
release:
	rm -rf $(REL_DIR)
	mkdir -p $(REL_DIR)
	cp -r $(VERILOGDIR) $(REL_DIR)
